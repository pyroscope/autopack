rTorrent-PS Auto Packaging
==========================

This continuously builds Debian packages for `rTorrent-PS`_ on different platforms,
using GitLab CI.

Please use the `GitHub issue tracker`_ for any issues related to this project.


.. _`rTorrent-PS`: https://github.com/pyroscope/rtorrent-ps
.. _`GitHub issue tracker`: https://github.com/pyroscope/rtorrent-ps/issues
